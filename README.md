# Các công nghệ sử dụng:
* Spring Boot 2.0.1
* Maven
* JDK 1.8
* Eclipse + Spring Tool Suite

Bean [httpSessionEventPublisher](https://docs.spring.io/spring-security/site/docs/4.2.7.RELEASE/apidocs/org/springframework/security/web/session/HttpSessionEventPublisher.html) để enable khả năng hỗ trợ concurrent session-control.

Method sessionManagement() dùng để quản lý session,

* Method *invalidSessionUrl()* dùng để chỉ định url sẽ chuyển hướng tới nếu request chứa session đã hết hạn
* Method *maxSessionsPreventsLogin()*: xác định số lượng session lớn nhất có thể hoạt động đồng thời, ở đây mình để là 1 tức là 1 tài khoản chỉ cho phép hoạt động tại một nơi duy nhất.
* Method *maxSessionsPreventsLogin()*: nếu tham số đầu vào là true thì không thể login ở nơi khác khi đã đạt max session, nếu bằng false thì cho phép login ở nơi khác còn nơi login trước đó sẽ bị hết hạn.
* Method *expiredUrl()*: chỉ định đường dẫn sẽ chuyển hướng trong trường hợp login thất bại do tình huống bị timeout do login ở nơi khác.
