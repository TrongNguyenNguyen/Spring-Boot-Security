package stackjava.com.sbmaxsession;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMaxSessionApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMaxSessionApplication.class, args);
	}
}
